import numpy as np
import os
import pandas as pd
import random
import spacy
import warnings

from datetime import datetime
from operator import itemgetter
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression

warnings.filterwarnings('ignore')


DIR = "../../github/quote-recommender/data/"
QUOTES = "quotes.csv"
GLOBAL_HISTORY = "global_testing.csv"
GLOBAL_HISTORY_SAVE = "global_testing.csv"
USER = "user_data.csv"
MIN_RANDOM_QUOTES = 3
MAX_QUOTE_SAMPLES = 5
RANDOM_STATE = 1919
TEXT_FOR_SIMILARITY = 'quote+author+tags'

# Setup
quotes_path = DIR + QUOTES
global_history_path = DIR + GLOBAL_HISTORY
global_history_save_path = DIR + GLOBAL_HISTORY_SAVE
user_data_path = DIR + USER


quotes_df = pd.read_csv(quotes_path)
global_history = pd.read_csv(global_history_path)


nlp = spacy.load('en_core_web_lg')
columns= ['text', 'author', 'categories', 'tags', 'quote+author', 'quote+tags',
          'quote+author+tags', 'quote_word_count', 'sentiment_score',
          'quote_index', 'user_id', 'neg_sim', 'pos_sim', 'timestamp',
          'user_feedback']
feedback_statement = "Let me know if you like that one and I'll keep trying to find quotes you like! (y/n)"
positive_feedback = ["good", 'great', 'love', 'y', 'yes', 'yeah', 'like', 'yep', 'yup', '+1', "liked"]
negative_feedback = ['bad', 'awful', 'hate', 'n', 'no', 'nah', 'dislike', 'nope', '-1']
anti_feedback = ["don't", "not", "no", "n't", "nt", "dont", "don"]
goodbye_feedback = ['bye', 'see ya', 'adios', 'goodbye', 'quit', 'q', 'cya', 'stop', 'end']
greeting_feedback = ['hi', 'hello', 'hola', 'greetings', 'sup', 'yo', "what's up"]

def similarity(text_vect, comp_text):
    similarities = []
    for text in text_vect:
        similarity = cos_sim(
                nlp(text).vector,
                nlp(comp_text).vector
            )
        similarities.append(similarity)
    return np.mean(similarities)


# Function to find cosine similarity of two variables
def cos_sim(a, b):
    return np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))


def add_symbol(tags, delimiter=',', symbol='#'):
        return ' '.join([f'{symbol}{word}' for word in tags.split(delimiter)])  

class User:
    def __init__(self, user_id):
        self.user_id = user_id

class QuoteConvo:
    def __init__(self, user, global_history):
        self.user = user
        self.global_history = global_history
        self.features = ['quote_word_count', 'sentiment_score', 'neg_sim', 'pos_sim']
        if global_history.shape[0] == 0:
            self.first_index = 0
        else:
            self.first_index = global_history.index[-1] + 1



    def goodbye_reply(self):
        """
        Function that is triggered when user indicates they would like to end the conversation by using
            values in the 'goodbye_feedback list above.
        Writes history of session to the global history csv and prints output of session history to user.
        """
        self.save_history()
        if self.global_history.shape[0] != 0:
            last_index = self.global_history.index[-1] + 1
            session_history = self.global_history.loc[self.first_index:last_index, ['text', 'author', 'tags', 'user_feedback']]
        else:
            session_history = ""
        print("bot: Bye!")
        if self.global_history.shape[0] > 0:
            print(session_history)
        
    def greeting_reply(self):
        """
        Function that is triggered when the User uses word within the 'greeting_feedback' list.
        """
        print("Hi! Let me know if you'd like to hear a quote.")

    def get_quote_sample(self):
        used_quotes = self.user_history['quote_index']
        mask = ~quotes_df.index.isin(used_quotes)
        filtered_df = quotes_df[mask]
        
        pos_df = self.global_history[self.global_history['user_feedback'] == 1]
        neg_df = self.global_history[self.global_history['user_feedback'] == 0]

        if len(pos_df) < MIN_RANDOM_QUOTES or len(neg_df) < MIN_RANDOM_QUOTES:
            quote = filtered_df.sample(n=1)
            quote['pos_sim'] = np.nan
            quote['neg_sim'] = np.nan
            quote['user_id'] = int(self.user.user_id)
            quote['timestamp'] = datetime.now()
            return quote

        if sum(self.user_history.isnull().sum()) > 0:
            for i in self.user_history.index:
                row = self.user_history.loc[i]

                pdf = pos_df[pos_df.index != i]
                pos_sim = similarity(pdf[TEXT_FOR_SIMILARITY], self.user_history.loc[i, TEXT_FOR_SIMILARITY])

                ndf = neg_df[neg_df.index != i]
                neg_sim = similarity(ndf[TEXT_FOR_SIMILARITY], self.user_history.loc[i, TEXT_FOR_SIMILARITY])
                row['pos_sim'] = pos_sim
                row['neg_sim'] = neg_sim
                self.global_history.loc[i] = row


        quote_sample = filtered_df.sample(n=MAX_QUOTE_SAMPLES)
        quote_sample.dropna(inplace=True)
        

        pos_df = self.global_history[self.global_history['user_feedback'] == 1]
        neg_df = self.global_history[self.global_history['user_feedback'] == 0]

        for i in quote_sample.index:
            random_quote = quote_sample.loc[i, :]
            if pos_df.shape[0] > 2:
                    
                pos_sim = similarity(pos_df[TEXT_FOR_SIMILARITY], quote_sample.loc[i, TEXT_FOR_SIMILARITY])
                quote_sample.loc[i, 'pos_sim']= pos_sim
            else:
                quote_sample.loc[i, 'pos_sim']= np.nan
                
            if neg_df.shape[0] > 2:
                neg_sim = similarity(neg_df[TEXT_FOR_SIMILARITY], quote_sample.loc[i, TEXT_FOR_SIMILARITY])
                quote_sample.loc[i, 'neg_sim']= neg_sim
            else:
                quote_sample.loc[i, 'pos_sim']= np.nan

        return quote_sample

    def predict(self, quote_sample, test_size=0.3, random_state=RANDOM_STATE):
        """
        Models and runs predictions on sample of quotes (quote_sample).
        Uses self.features for independent variables in model.
        Returns a single quote with a prediction of being liked, and has the highest probability
             of being correct and is predited to be liked by user.
        """
        X = self.global_history[self.features]
        y = self.global_history['user_feedback'].astype(int)

        X_train, X_test, y_train, y_test = train_test_split(
            X,
            y,
            test_size=test_size,
            stratify=y,
            random_state=random_state
        )

        lr = LogisticRegression()
        lr.fit(X_train, y_train)
        
        # TO-DO: double check this returns a prediction of '1' with highest probability of being correct.
        X_preds = quote_sample[self.features].copy()
        proba_preds = lr.predict_proba(X_preds)

        # Create list of nX2, where column 1 is predicted value (0 or 1) and column 2 is the probability
        probs = [[X_preds.index[i], prob[1]] for i, prob in enumerate(proba_preds)]
        probs = sorted(probs, key=itemgetter(1))
        indx = probs[-1][0]
        
        predicted_quote = quote_sample.loc[indx].copy()
        predicted_quote['user_id'] = self.user.user_id
        predicted_quote['timestamp'] = datetime.now()

        return predicted_quote


    def quote_reply(self):
        quote_sample = self.get_quote_sample()
        if len(quote_sample.index) < 2:
            predicted_quote = quote_sample.iloc[0]
        else:
            predicted_quote = self.predict(quote_sample, test_size=0.3, random_state=RANDOM_STATE)
        quote = predicted_quote['text']
        author = predicted_quote['author']
        tags  = add_symbol(predicted_quote['categories'])
        reply = f"bot:...\n\n{quote}\n-{author}  {tags}\n\n{feedback_statement}\n"
        print(reply)
        
        user_reply = ""
        
        while True:
            user_reply = input("YOU: ").lower()
            if user_reply in positive_feedback:
                predicted_quote['user_feedback'] = 1
                print("Awesome! Glad you liked it.")
                return predicted_quote
            elif user_reply in negative_feedback:
                predicted_quote['user_feedback'] = 0
                print("Got it - thanks for the feedback!")
                return predicted_quote
            elif user_reply in goodbye_feedback:
                return 
            else:
                print("bot: I'm sorry - I didn't get that. Please let me know if you liked the quote (y/n).")
                
    def update_history(self, quote):
        self.global_history = self.global_history.append(quote, ignore_index=True)
        
    def save_history(self):
        self.global_history.to_csv(global_history_save_path, index=False)
    
    @property
    def user_history(self):
        if self.global_history[self.global_history['user_id'] == self.user.user_id].shape[0] > 0:
            return self.global_history[self.global_history['user_id'] == self.user.user_id]
        else:
            return self.global_history.copy()

     
    def conversation(self):
        print("bot: Hello! I am here to help find quotes you may like. Please type 'quote' if you would like to hear a quote.")
        
        user_reply = ""
        while True:
            user_reply = [word.text for word in nlp(input("YOU: ").lower())]

            if any(word in user_reply for word in ['quote', 'quotes']):
                if any(word in user_reply for word in anti_feedback):
                    print("OK no problem. Let me know when you'd like to hear a quote.")
                else:
                    quote = self.quote_reply()

                    if not isinstance(quote, pd.Series):
                        self.goodbye_reply()
                        break
                    else:
                        self.update_history(quote)
                        
            elif any(word in user_reply for word in greeting_feedback):
                self.greeting_reply()

            elif any(word in user_reply for word in goodbye_feedback):
                self.goodbye_reply()
                break        

if os.path.isfile(user_data_path):
    user_data = pd.read_csv(user_data_path)
    user = User(user_data['user_id'].values[0])
else:
    user_id = np.sort(global_history['user_id'])[-1] + 1
    user_data = pd.DataFrame({'user_id': [user_id]})
    user = User(user_id)
    user_data.to_csv(user_data_path ,index=False)

convo = QuoteConvo(user, global_history)
convo.conversation()
print("Hello")